#include "stdafx.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <process.h>
#include <atomic>
#include <queue>
#include <mutex>
using namespace std;

short m_nDevIndex;
short m_nCalData[32];
int m_nCH1VoltDIV;
int m_nCH2VoltDIV;
int m_nTimeDIV;
ULONG nReadLen = 1000000;//102400;//10k

const int LOW_V = 0;
const int HIGH_V = 100;

struct pulse {
	bool v;
	int len;
	pulse(){};
	pulse(int _v, int _len) : v(_v), len(_len) {};
};

const int LONG_I[] = {740, 830};
const int SHORT_I[] = {340, 430};
const int PREFIX_I[] = {3700, 4100};

inline bool inside(int v, const int* B) {
	return v >= B[0] && v <= B[1];
}

inline bool isLong(int v) {
	return inside(v, LONG_I);
}

inline bool isShort(int v) {
	return inside(v, SHORT_I);
}

inline bool isPrefixGap(int v) {
	return inside(v, PREFIX_I);
}

inline bool isValid(int v) {
	return isLong(v) || isShort(v);
}

struct code {
	int prefixLen; // should == 23
	vector<bool> payload;

	void add(bool bit) {
		payload.push_back(bit);
	}

	bool operator!=(const code& other) {
		if (payload.size() < 2 || other.payload.size() < 2) return true;
		vector<bool> tmp(payload);
		vector<bool> tmp2(other.payload);
		tmp.pop_back();
		tmp2.pop_back();
		return tmp != tmp2;
	}
};

bool extract(const vector<pulse>& T, int& i, code& C) {
	bool v = false;
	int oors = 0;
	int n = T.size();

	while(i < n && ! isValid(T[i].len)) {
		++i;
		++oors;
	}

	if (i >= n) return false;

	int prefix = 0;

	while(i < n && isShort(T[i].len)) {
		++i;
		++prefix;
	}

	if (i >= n) return false;
	if (prefix < 4) {
		++i;
		return false;
	}
	C.prefixLen = prefix;


	if (T[i].v == 0 && isPrefixGap(T[i].len)) {
		++i;
	} else {
		return false;
	}

	while (i+i < n && isValid(T[i].len)) {
		if ( T[i].v != 1 || T[i+1].v != 0 ) break;
		if ( isLong(T[i].len) && ! isLong(T[i+1].len)) {
			C.add(1);
			if (! isShort(T[i+1].len) ) break;
		} else if ( isShort(T[i].len) && ! isShort(T[i+1].len) ) {
			C.add(0);
			if ( ! isLong(T[i+1].len) ) break;
		} else break;
		i += 2;
	}

	return C.payload.size() == 66;
}

short* getMoarData(){
	ULONG nDrawLen = nReadLen; //1; //1024000;// / 2;//100000; ???

	short* pCH1Data;
	short* pCH2Data;

	short nTrigLevel = 0;
	short nSlope = 0;// 0:Rise; 1: Fall
	short nHTrigPos = 0;// 0 ~ 100
	ULONG nTrigPoint = 0;

	pCH1Data = new short[nReadLen];
	pCH2Data = new short[nReadLen];		

	short nRe = dsoReadHardData(m_nDevIndex,
								pCH1Data, 
								pCH2Data,
								nReadLen,
								m_nCalData,
								m_nCH1VoltDIV,
								m_nCH2VoltDIV,
								0,//0:AUOT; 1:Normal; 2: Signal
								0,//CH1
								nTrigLevel,
								nSlope,
								m_nTimeDIV,
								nHTrigPos,
								nDrawLen, // <--- wtf?
								&nTrigPoint,
								0);
	
	delete pCH2Data;

	if (nRe > 0) {
		return pCH1Data;
	}

	delete pCH1Data;
	return NULL;
}

inline int clip(int value) {
	return value > (LOW_V + HIGH_V / 2);
}

// parse.py get_interval_repr
vector<pulse> parse(short* data, int n) {
	int same = 0;
	vector<pulse> R;
	for(int i=1; i<=n; ++i) {
		if (i < n && clip(data[i-1]) == clip(data[i])) ++same;
		else {
			R.push_back(pulse(clip(data[i-1]), same));
			same = 0;
		}
	}
	return R;
}

int codesFound = 0;
int chunksRecorded = 0;
int queueSize = 0;
int recordFailures = 0;
int codeOccurences = 0;

queue<short*> Q;
mutex qm;

void printStatus() {
	cout << "\r C: " << codesFound << "\t/" << codeOccurences << "\tChunks: " << chunksRecorded << "\tRecF: " << recordFailures << "\tQ: " << queueSize << "   ";
}

void recordingLoop() {
	while(true) {
		short* data = getMoarData();
		if (data == NULL) {
			++recordFailures;
		} else {
			qm.lock();
			Q.push(data);
			queueSize = Q.size();
			printStatus();
			qm.unlock();
			++chunksRecorded;
			
		}
	}
}

void parsingLoop(void *param) {
	
	int n = nReadLen;

	std::ofstream F;
	F.open("codes.txt", ios_base::app);
	
	SYSTEMTIME st;
    GetSystemTime(&st);
   
	F << "# new recording session " << st.wYear << "/" << st.wMonth << "/" << st.wDay << " " << st.wHour << ":" << st.wMinute << ":" << st.wSecond << endl;
	F << flush;

	code lastCode;
	int idleFor = 0;

	while(true) {
		
		short* data = NULL;

		{
			qm.lock();
			printStatus();
			if (! Q.empty()) {
				data = Q.front();
				Q.pop();
			}
			queueSize = Q.size();
			printStatus();
			qm.unlock();
		}

		if (data == NULL) {
			Sleep(100);
			continue;
		}

		++idleFor;

		vector<pulse> P(parse(data, n));
		delete[] data;

		int i = 0;
		
		while (i < P.size()) {
			code C;
			if ( extract(P, i, C) ) {
				idleFor = 0;
				++codeOccurences;
				if (C != lastCode) {
					++codesFound;
					lastCode = C;
					for(bool b: C.payload) {
						F << ( (int) b );
					}
					F << endl;

				}
			}
		}

		if (idleFor == 15) {
			MessageBox(NULL, (LPCWSTR)L"MISSING READINGS", (LPCWSTR)L"ACTION REQUIRED", 0x00001000L);
		}
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	cout << "Hi!" << endl;

	// TODO: Add extra initialization here
	m_nDevIndex = 0;
	m_nCH1VoltDIV = 5;//1V/DIV
	m_nCH2VoltDIV = 5;//1V/DIV
	m_nTimeDIV = 24;//1MSa/s

	cout << "Opening device" << endl;

	if(dsoOpenDevice(0))
	{
		dsoGetCalLevel(m_nDevIndex,m_nCalData,32);
		dsoSetVoltDIV(m_nDevIndex,0,m_nCH1VoltDIV);
		dsoSetVoltDIV(m_nDevIndex,1,m_nCH2VoltDIV);
		dsoSetTimeDIV(m_nDevIndex,m_nTimeDIV);
		
		cout << "Device configured" << endl;
	}
	else
	{
		cout << "No device was found! Exiting." << endl;
		system("pause");
		return 0;
	}

	system("pause");

	_beginthread(parsingLoop, 0, NULL);
	recordingLoop();
}

