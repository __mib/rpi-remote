#!/usr/bin/env python
import cgi
import random
import os
import sys

GAP = '- 4000\n'
HEADER_SPIKE = '+ 400\n- 400\n'
ONE = '+ 800\n- 400\n'
ZERO = '+ 400\n- 800\n'
DATA = {
	'0': ZERO, 
	'1': ONE
}

def getCode():
    markf = open('gatemark.txt', 'r')
    mark = markf.readlines()[0].strip()
    markf.close()

    print "GATEMARK", mark

    codesf = open('codes.txt', 'r')
    codes = [x.strip()[:-1] for x in codesf if len(x) > 0 and x[0] != '#']
    codesf.close()

    print "CODES ready"

    code = codes[codes.index(mark) + 1]
    markf = open('gatemark.txt', 'w')
    markf.write(code)
    markf.close()

    batchf = open('batch.txt', 'w')
    batchf.write('c 0\n')

    def gen(batchf, code, reps):
        for x in range(12):
            batchf.write(HEADER_SPIKE)

        batchf.write(GAP)


        for bit in code:
            batchf.write(DATA[bit])

        batchf.write('e ' + str(reps) + '\n')

    #batchf.write('c 0\n- 16000\n')

    #gen(batchf, code + '1', 1)

    batchf.write('c 0\n- 16000\n')

    gen(batchf, code + '0', 10)

    batchf.close()

args = cgi.FieldStorage()

key = str(random.randint(0, 1000000))

print 'Content-Type: text/html'
print "Set-Cookie: key={}".format(key)
print ''

if 'type' in args:
    if "HTTP_COOKIE" not in os.environ:
        print ''
        print "NO COOKIES??!"
        exit()
    print ''
    k, v = os.environ["HTTP_COOKIE"].split('=')
    if k != 'key':
        exit()
    if v != args.getvalue('key'):
        print 'KEY MISMATCH'
        exit()

    if args.getvalue('type') == 'gate':
        getCode()
        os.system('cat batch.txt > /dev/mib')
    elif args.getvalue('type') == 'psw':
        name = args.getvalue('number') + args.getvalue('action') + '.ctl'
        os.system("cat ctl/" + name + " > /dev/mib")

print """
<table border="1" width="100%">
"""


print """
    <tr>
    <td colspan="2">
        <a href="?type=gate&key={0}" style="display:block;"><center><h1>GATE</h1></center></a>
    </td>
  </tr>
""".format(key)

for number in range(1, 5):
    print "<tr>"
    for action in ['ON', 'OFF']:
        print """
            <td>
            <a href="?type=psw&key={0}&number={1}&action={2}" style="display:block;"><center><h1>PSW_{1}_{2}</h1></center></a></td>
        """.format(key, number, action)
    print "</tr>"
print "</table>"


