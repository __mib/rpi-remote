import sys
f = open(sys.argv[1])

LONG = range(750, 820)
SHORT = range(350, 420)
PREFIX_GAP = range(3800, 4200)

T = []
L = []
S = []

for line in f:
    try:
        c, i = line.split(' ')
        T.append((c, int(i)))
    except:
        print "line not parsed:", line

svals = []
lvals = []

for c, i in T:
    desc = '?'
    if i in LONG:
        desc = 'L'
        lvals.append(i)
    if i in SHORT:
        desc = 'S'
        svals.append(i)
#    print c, i, desc
    S.append(c)
    L.append(i)

#print "Average SHORT:", sum(svals) / len(svals)
#print "Average LONG:", sum(lvals) / len(lvals)



def extract(L, S, i):
    # rewind the header
    v = False
    oors = 0
    while i < len(L) and L[i] not in (SHORT + LONG):
        i+=1
        oors+=1

    if v: print "Skipping first", oors, "entries"

    if i >= len(L):
        return i, None

    prefix = 0
    while i < len(L) and L[i] in SHORT:
        i+=1
        prefix+=1

    if v: print "Found", prefix, "prefix"
    
    if i >= len(L):
        return i, None
    
    if prefix != 23:
        if v: print "WARNING: prefix len", prefix

    if prefix < 2:
        return i+1, None

    if S[i] == '-' and L[i] in PREFIX_GAP:
        i+=1
        if v: print "Prefix gap found"
    else:
        if v: print "Prefix gap missing"
        return i, None

    payload = []

    while i+1 < len(T) and L[i] in (SHORT + LONG):
        if S[i] != '+' or S[i+1] != '-':
            if v: print "Wrong signs"
            break
        if L[i] in LONG and L[i+1] not in LONG:
            payload.append(1)
            if L[i+1] not in SHORT:
                if v: print "Accounting for the last bit"
                break
        elif L[i] in SHORT and L[i+1] not in SHORT:
            payload.append(0)
            if L[i+1] not in LONG:
                if v: print "Accounting for the last bit"
                break
        else:
            break
        i+=2

    if v: print "Payload", payload
    if v: print "Payload len", len(payload)
    
    #print prefix 
    return i, payload

i = 0
while i < len(T):
    ni, p = extract(L, S, i)
    if p:
        print p
    if ni <= i:
        print "ups..."
    i = ni
