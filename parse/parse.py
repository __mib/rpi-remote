import sys, struct
import matplotlib.pyplot as plt
import numpy as np

CLOCK_PREFIX = "#CLOCK="

def read(lines):
	clock = None
	values = []
	
	for line in lines:
		line = line.strip()
		if not len(line): continue
		
		if line[0] == '#':
			if line.startswith(CLOCK_PREFIX):
				clock_with_unit = line[len(CLOCK_PREFIX):]
				if not clock_with_unit.endswith("mS"):
					raise Exception("Clock not in mS")
				
				clock = float(clock_with_unit[:-2])
		
		try:
			value = float(line)
			values.append(value)
		except ValueError:
			pass
	return values, clock

def guess_voltage_levels(values):
	bins, edges = np.histogram(values, 50, normed=0)
	#left, right = edges[:-1],edges[1:]
	#X = np.array([left, right]).T.flatten()
	#Y = np.array([bins, bins]).T.flatten()
	#plt.plot(X,Y)
	#plt.show()
	
	a, b = sorted(zip(bins, edges), reverse=True)[:2]
	if a[1] > b[1]:
		a, b = b, a
	
	return a[1], b[1]
	
def clip_to_state(values, low, high):
	m = (low + high) / 2
	clipped = []
	for v in values:
		if v < m:
			clipped.append(0)
		else:
			clipped.append(1)
	return clipped
	
def get_interval_repr(values, clock):
	same = 0
	R = []
	for x in range(1, len(values)):
		if values[x-1] == values[x] and x < len(values)-1:
			same += 1
		else:
			R.append((values[x-1], same*clock))
			same = 0
	return R
	
def main():
	fname = sys.argv[1]
	f = open(fname, 'r')
	values, clock = read(f.readlines())

	if len(sys.argv) > 2:
		a, b = int(sys.argv[2]), int(sys.argv[3])
		values = values[a:b]
	
	print "Read", len(values), "datapoints; clock=", clock

	low, high = guess_voltage_levels(values)
	print "Assuming low state at:", low, "high state at", high
	
	values = clip_to_state(values, low, high)

	#plt.plot([x*clock for x in range(0, len(values))], values)
	x = range(0, len(values))
	plt.fill_between(x, values, 0, color = '0.9')
	plt.plot(x, values)
	
	plt.show()
	
	clock = 1.0 #?
	intervals = get_interval_repr(values, clock)
	for s, t in intervals:
		s = s and '+' or '-'
		print s, int(t)

	#readings_filename = sys.argv[1]
	#f = open(readings_filename, 'r')
	
if __name__ == "__main__":
    main()
