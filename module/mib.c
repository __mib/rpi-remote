/* You probably shouldn't use this module unless you're me. */

#include <linux/blkdev.h>
#include <linux/cdev.h>
#include <linux/module.h>

static dev_t dev_num;
static struct cdev c_dev;
static struct class *cl;

static void __iomem *gpio;

#define TX_PIN              24
#define BCM2708_PERI_BASE   0x20000000
#define GPIO_BASE           (BCM2708_PERI_BASE + 0x200000)

#define MAX_mib_line 30
#define MAX_OPS 100000

#define MIB_CMD_BUF_SIZE 256
#define MAX_QUEUE_SIZE 100*1024
static char mib_line[MIB_CMD_BUF_SIZE + 1];
static char mib_cmd_buf[MIB_CMD_BUF_SIZE + 1];

static int mib_q_head;
static int mib_queue_len[MAX_QUEUE_SIZE];
static char mib_queue_state[MAX_QUEUE_SIZE];

/* http://elinux.org/Rpi_Low-level_peripherals

// GPIO setup macros. Always use INP_GPIO(x) before using OUT_GPIO(x) or SET_GPIO_ALT(x,y)
#define INP_GPIO(g) *(gpio+((g)/10)) &= ~(7<<(((g)%10)*3))
#define OUT_GPIO(g) *(gpio+((g)/10)) |=  (1<<(((g)%10)*3))
#define SET_GPIO_ALT(g,a) *(gpio+(((g)/10))) |= (((a)<=3?(a)+4:(a)==4?3:2)<<(((g)%10)*3))

#define GPIO_SET *(gpio+7)  // sets   bits which are 1 ignores bits which are 0
#define GPIO_CLR *(gpio+10) // clears bits which are 1 ignores bits which are 0
*/

inline uintptr_t get_addr(unsigned g) {
  return ((uintptr_t) gpio) + sizeof(uintptr_t) * (g / 10);
}

inline void INP_GPIO(unsigned g) {
  uintptr_t addr = get_addr(g);
  iowrite32(ioread32(addr) & ~(7<<(((g)%10)*3)), addr);
}

inline void OUT_GPIO(unsigned g) {
  uintptr_t addr = get_addr(g);
  iowrite32(ioread32(addr) | (1<<(((g)%10)*3)), addr);
}

inline void GPIO_SET(unsigned g) {
  iowrite32(g, (uintptr_t) gpio + (4 * 7));
}

inline void GPIO_CLR(unsigned g) {
  iowrite32(g, (uintptr_t) gpio + (4 * 10));
}

void sleep(unsigned int micros) {
    struct timeval t;
    unsigned int start_sec, start_usec;
    do_gettimeofday(&t);

    start_sec = t.tv_sec;
    start_usec = t.tv_usec;
    while(true) {
      do_gettimeofday(&t);
      if ( (t.tv_sec - start_sec) * 1000000 + t.tv_usec - start_usec >= micros ) return;
    }
}

void execute_queue(void) {
    int i;
    unsigned long flags;

    local_irq_save(flags);
    
    for (i=0; i<mib_q_head; ++i) {
        if (mib_queue_state[i] == '+') {
            GPIO_SET(1 << TX_PIN);
            sleep(mib_queue_len[i]);
            GPIO_CLR(1 << TX_PIN);
        } else {
            GPIO_CLR(1 << TX_PIN);
            sleep(mib_queue_len[i]);
        }
    }

    local_irq_restore(flags);
}

static int mib_open(struct inode *i, struct file *f) {
  printk(KERN_INFO "MibDeviceDriver: open()\n");
  return 0;
}

static int mib_close(struct inode *i, struct file *f) {
  printk(KERN_INFO "MibDeviceDriver: close()\n");
  return 0;
}

static ssize_t mib_read(struct file *f, char __user *buf, size_t len, loff_t *off) {
  printk(KERN_INFO "MibDeviceDriver: read() q_head %d\n", mib_q_head);
  return 0;
}

static ssize_t mib_write(struct file *f, const char __user *buf, size_t len, loff_t *off) {
  int value = 0; int bread = 0; int i; int cread = 0;
  

  printk(KERN_INFO "MibDeviceDriver: write()\n");

  mib_cmd_buf[0] = 'x';

  printk(KERN_INFO "MibDeviceDriver: write(): len %d", len);
  

  if (len <= 0) {
    printk(KERN_INFO "MibDeviceDriver: write(): len %d > MIB_CMD_BUF_SIZE(%d)\n", len, MIB_CMD_BUF_SIZE);
    return -EINVAL;
  }

  if (len >= MIB_CMD_BUF_SIZE) {
      len = MIB_CMD_BUF_SIZE - 1;
      printk(KERN_INFO "MibDeviceDriver: write(): truncated len %d", len);
  }

  if (copy_from_user(mib_line, buf, len) != 0) {
    printk(KERN_INFO "MibDeviceDriver: write(): copy_from_user error\n");
    return -EINVAL;
  }


  mib_line[len] = 0;



  printk(KERN_INFO "MibDeviceDriver: write(): %s\n", mib_line);
  
  bread = sscanf(mib_line, "%s %d%n", mib_cmd_buf, &value, &cread);
  printk(KERN_INFO "MibDeviceDriver: write(): sscanf returns %d\n", bread);
  len = cread;

  if (bread != 2) {
    if (mib_line[0] == ' ' || mib_line[0] == 0 || mib_line[0] == '\n') return 1;
    return -EINVAL;
  }
  printk(KERN_INFO "MibDeviceDriver: command: %s value %d\n", mib_cmd_buf, value);

  //return len;


  if (mib_cmd_buf[0] == 'c') {
      printk(KERN_INFO "MibDeviceDriver: clear\n");
      mib_q_head = 0;
      // return -EINVAL;
  } else if (mib_cmd_buf[0] == '+' || mib_cmd_buf[0] == '-') {
      mib_queue_len[mib_q_head] = value;
      mib_queue_state[mib_q_head] = mib_cmd_buf[0];
      ++mib_q_head;
  } else if (mib_cmd_buf[0] == 'p') {
      printk(KERN_INFO "MibDeviceDriver: queue state; length: %d\n", mib_q_head);
      for(i=0; i<mib_q_head; ++i) {
          printk(KERN_INFO "MibDeviceDriver: queue[%d] state %c; length: %d\n", i, mib_queue_state[i], mib_queue_len[i]);
      }
  } else if (mib_cmd_buf[0] == 'e') {
      printk(KERN_INFO "MibDeviceDriver: executing\n");
      if (value > 0) {
        while(value--)
          execute_queue();
      }
  }

  //*off += len;
  //return -EINVAL;
  return len;
}

static struct file_operations fops = {
  .owner = THIS_MODULE,
  .open = mib_open,
  .release = mib_close,
  .read = mib_read,
  .write = mib_write
};

static int __init mib_init(void) {
  printk(KERN_INFO "MibDeviceDriver: init()\n");

  gpio = ioremap_nocache(GPIO_BASE, BLOCK_SIZE);
  if (gpio == NULL) {
      printk(KERN_ERR ": ioremap_nocache failed\n");
      return -ENXIO;
  }

  INP_GPIO(TX_PIN);
  OUT_GPIO(TX_PIN);

  if (alloc_chrdev_region(&dev_num, 0, 1, "mib") < 0) {
    printk(KERN_INFO "MibDeviceDriver: alloc_chrdev_region failure\n");
    return -1;
  }
  printk(KERN_INFO "MibDeviceDriver: char device registered\n");
  
  cl = class_create(THIS_MODULE, "chardrv");
  if (cl == NULL) {
    printk(KERN_INFO "MibDeviceDriver: class_create failure\n");
    unregister_chrdev_region(dev_num, 1);
    return -1;
  }
  printk(KERN_INFO "MibDeviceDriver: class created\n");


  if (device_create(cl, NULL, dev_num, NULL, "mib") == NULL) {
    printk(KERN_INFO "MibDeviceDriver: device_create failure\n");
    class_destroy(cl);
    unregister_chrdev_region(dev_num, 1);
    return -1;
  }
  printk(KERN_INFO "MibDeviceDriver: device created\n");

  cdev_init(&c_dev, &fops);
  
  if (cdev_add(&c_dev, dev_num, 1) < 0) {
    printk(KERN_INFO "MibDeviceDriver: cdev_add failure\n");
    device_destroy(cl, dev_num);
    class_destroy(cl);
    unregister_chrdev_region(dev_num, 1);
    return -1;
  }
  printk(KERN_INFO "MibDeviceDriver: char device added to the system\n");

  //mib_q_head = 0;
  printk(KERN_INFO "MibDeviceDriver: ready for action\n");

  return 0;
}
 
static void mib_exit(void) {
  printk(KERN_INFO "MibDeviceDriver: exit()");

  cdev_del(&c_dev);
  device_destroy(cl, dev_num);
  class_destroy(cl);
  unregister_chrdev_region(dev_num, 1);

  printk(KERN_INFO "MibDeviceDriver: device unregistered");
}
 
subsys_initcall(mib_init);
module_exit(mib_exit);

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("mib");
MODULE_DESCRIPTION("Yet another RPI GPIO driver");
